#Comment your scripts!
#
#Let's import the librairies
import os
import mailparser
import re
import csv #we import the csv library
#Let's declare where the mails are on our disc
directory = '/home/herve/Téléchargements/Vitaly_Kovalchuk_Email_Leak/'
#Let's do a loop
for file in os.listdir(directory):
  mail = mailparser.parse_from_file(os.path.join(directory,file))
  TheDate = mail.date
  TheTo = mail.to
  TheFrom = mail.from_
  try : 
    TheIP = str(mail.headers) #grab the full headers and returns a string to be parsed
    TheIP = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', TheIP).group()
  except:
    TheIP = None
  #print(TheFrom,TheTo,TheDate,TheIP) we keep this information in case of debugging
  with open('email.csv', 'a') as newfile:
    newfileWriter = csv.writer(newfile)
    newfileWriter.writerow([TheFrom,TheTo,TheDate,TheIP])
