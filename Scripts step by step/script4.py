#Comment your scripts!
#
#Let's import the librairies
import os
import mailparser
import re
import csv
from geoip import geolite2
#Let's declare where the mails are on our disc
directory = '/home/herve/Téléchargements/Vitaly_Kovalchuk_Email_Leak/'
#Let's do a loop
for file in os.listdir(directory):
  mail = mailparser.parse_from_file(os.path.join(directory,file))
  TheDate = mail.date
  TheTo = mail.to
  TheFrom = mail.from_
  TheSubject = mail.subject #we had the subject
  try : 
    TheIP = str(mail.headers) #grab the full headers and returns a string to be parsed
    TheIP = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', TheIP).group()
  except:
    TheIP = None
  TheEnrich = TheIP
  try : 
    match = geolite2.lookup(TheEnrich)
    TheCountry = match.country
  except:
    match = None
    TheCountry = None
  #print(TheFrom,TheTo,TheDate,TheIP) we keep this information in case of debugging
  with open('email.csv', 'a') as newfile:
    newfileWriter = csv.writer(newfile)
    newfileWriter.writerow([file,TheFrom,TheTo,TheSubject,TheDate,TheIP,TheCountry])#we also had the name of the file and the subject
