#Dataharvest 2019 - Investigating with Python and OSINT

The purpose of this workshop is to explain to absolute beginners in python, how this language can help you work faster in your investigations.
More than the language itself, it'll show you how to assembly code parts found on the internet to achieve your goal.
To illustrate this technique, we'll base this workshops on a case study : the email leaks of Vitaly Kovalchuk. 
https://roguemedialabs.com/wp-content/uploads/2019/03/Vitaly_Kovalchuk_Email_Leak.zip

Keypoints : search, vocabulary, python.

Pre-requisites : Python3 on the machine. A browser. A text editor.


